# tarefa1-git

import java.util.Scanner;
public class Menu {
    public static void main(String[] args) {
        int opcao, selecione;
        selecione = 0;

        do {
            Scanner menu = new Scanner(System.in);

            System.out.println("|          MENU         |");
            System.out.println("|    Opções:            |");
            System.out.println("|    1. Opção 1         |");
            System.out.println("|    2. Opção 2         |");
            System.out.println("|    3. Sair            |");
            System.out.println(" Selecione uma opção: ");
            opcao = menu.nextInt();

            switch (opcao) {
                case 1:
                    System.out.println(" Você escolheu a primeira opção ");
                    break;
                case 2:
                    System.out.println(" Você escolheu a segunda opção ");
                    break;
                case 3:
                 selecione = 3;
                    System.out.println("Sair - O programa foi encerrado");
                break;
                default:
                    System.out.println(" Seleção inválida");
                    break;
                }
        } while (selecione != 3);

    }
}